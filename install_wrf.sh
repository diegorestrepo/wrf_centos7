#!/bin/bash


# * ------------------------------------------------------------------------------------
# * "THE BEER-WARE LICENSE" (Revision 42):
# * <diegorestrepoleal@gmail.com> wrote this file. As long as you retain this notice you
# * can do whatever you want with this stuff. If we meet some day, and you think
# * this stuff is worth it, you can buy me a beer in return Diego Andrés Restrepo Leal.
# * ------------------------------------------------------------------------------------


clear

AZUL=$(tput setaf 6)
VERDE=$(tput setaf 2)
ROJO=$(tput setaf 1)
LILA=$(tput setaf 5)
LIMPIAR=$(tput sgr 0)


echo
echo "$VERDE=================================================$LIMPIAR"
echo "$VERDE=                    START                      =$LIMPIAR"
echo "$VERDE=================================================$LIMPIAR"
echo


echo
echo "$AZUL Insert user $LIMPIAR"
echo
read usuario
cd /home/$usuario
pwd


echo
echo "$AZUL Install perl $LIMPIAR"
echo
yum install -y perl


echo
echo "$AZUL Enable CentOS-Base $LIMPIAR"
echo
perl -pi -e "s/enabled=0/enabled=1/" /etc/yum.repos.d/CentOS-Base.repo


echo
echo "$AZUL Install epel-release $LIMPIAR"
echo
yum install -y epel-release


echo
echo "$AZUL Install nano $LIMPIAR"
echo
yum install -y nano


echo
echo "$AZUL Install wget $LIMPIAR"
echo
yum install -y wget


echo
echo "$AZUL Install tmux $LIMPIAR"
echo
yum install -y tmux


echo
echo "$AZUL Development tools $LIMPIAR"
echo
yum groupinstall -y 'Development Tools'


echo
echo "$AZUL Install tcsh $LIMPIAR"
echo
yum install -y tcsh


echo
echo "$AZUL Install m4 $LIMPIAR"
echo
yum install -y m4


echo
echo "$AZUL Install time $LIMPIAR"
echo
yum install -y time


echo
echo "$AZUL Create directory Build_wrf and TEST $LIMPIAR"
echo

if [ ! -d Build_WRF ];
then
    mkdir Build_WRF
fi

if [ ! -d TEST ];
then
    mkdir TEST
fi

ls


echo
echo "$AZUL Check compilers $LIMPIAR"
echo
for compiler in gfortran gcc cpp g++
do
    echo
    echo "Compiler: $compiler"
    which $compiler
    echo
done


echo
echo "$AZUL Test FORTRAN and C $LIMPIAR"
echo
cd TEST
wget http://www2.mmm.ucar.edu/wrf/OnLineTutorial/compile_tutorial/tar_files/Fortran_C_tests.tar
tar -xvf Fortran_C_tests.tar

echo "$LILA Test 1: Fixed Format Fortran Test. $LIMPIAR"
gfortran TEST_1_fortran_only_fixed.f
./a.out

echo "$LILA Test 2: Free Format Fortran. $LIMPIAR"
gfortran TEST_2_fortran_only_free.f90
./a.out

echo "$LILA Test 3: C. $LIMPIAR"
gfortran TEST_2_fortran_only_free.f90
./a.out

echo "$LILA Test 4: Fortran Calling a C Function (our gcc and gfortran have different defaults, so we force both to always use 64 bit [-m64] when combining them). $LIMPIAR"
gcc -c -m64 TEST_4_fortran+c_c.c
gfortran -c -m64 TEST_4_fortran+c_f.f90
gfortran -m64 TEST_4_fortran+c_f.o TEST_4_fortran+c_c.o
./a.out

echo "$LILA Test 5: csh. $LIMPIAR"
csh ./TEST_csh.csh

echo "$LILA Test 6: perl. $LIMPIAR"
./TEST_perl.pl

echo "$LILA Test 7: sh. $LIMPIAR"
./TEST_sh.sh

echo "$LILA Check command $LIMIPAR"
for command in ar head sed awk hostname sleep cat ln sort ls tar cp make touch cut mkdir tr expr mv uname file nm wc grep printf which gzip rm m4
do
    echo
    echo "Command: $command"
    which $command
    echo
done

cd ..


echo
echo "$AZUL Building Libraries $LIMPIAR"
echo
cd Build_WRF

if [ ! -d LIBRARIES ];
then
    mkdir LIBRARIES
fi

ls

cd LIBRARIES

echo "$LILA Download: mpich $LIMPIAR"
wget -nc https://www2.mmm.ucar.edu/wrf/OnLineTutorial/compile_tutorial/tar_files/mpich-3.0.4.tar.gz

echo "$LILA Download: netcdf $LIMPIAR"
wget -nc https://www2.mmm.ucar.edu/wrf/OnLineTutorial/compile_tutorial/tar_files/netcdf-4.1.3.tar.gz

echo "$LILA Download: Jasper $LIMPIAR"
wget -nc https://www2.mmm.ucar.edu/wrf/OnLineTutorial/compile_tutorial/tar_files/jasper-1.900.1.tar.gz

echo "$LILA Download: libpng $LIMPIAR"
wget -nc https://www2.mmm.ucar.edu/wrf/OnLineTutorial/compile_tutorial/tar_files/libpng-1.2.50.tar.gz

echo "$LILA Download: zlib $LIMPIAR"
wget -nc https://www2.mmm.ucar.edu/wrf/OnLineTutorial/compile_tutorial/tar_files/zlib-1.2.7.tar.gz

ls

echo
echo "$AZUL Environment variables $LIMPIAR"
echo
echo "export DIR=/home/$usuario/Build_WRF/LIBRARIES" >> /home/$usuario/.bashrc
echo 'export CC=gcc' >> /home/$usuario/.bashrc
echo 'export FC=gfortran' >> /home/$usuario/.bashrc
echo 'export FCFLAGS=-m64' >> /home/$usuario/.bashrc
echo 'export F77=gfortran' >> /home/$usuario/.bashrc
echo 'export FFLAGS=-m64' >> /home/$usuario/.bashrc
echo 'export JASPERLIB=$DIR/grib2/lib' >> /home/$usuario/.bashrc
echo 'export JASPERINC=$DIR/grib2/include' >> /home/$usuario/.bashrc
echo 'export LDFLAGS=-L$DIR/grib2/lib' >> /home/$usuario/.bashrc
echo 'export CPPFLAGS=-I$DIR/grib2/include' >> /home/$usuario/.bashrc
echo 'export PATH=$DIR/netcdf/bin:$PATH' >> /home/$usuario/.bashrc
echo 'export NETCDF=$DIR/netcdf' >> /home/$usuario/.bashrc
echo 'export PATH=$DIR/mpich/bin:$PATH' >> /home/$usuario/.bashrc
echo 'export LD_LIBRARY_PATH=$DIR/mpich/lib:$LD_LIBRARY_PATH' >> /home/$usuario/.bashrc
source /home/$usuario/.bashrc


echo
echo "$AZUL Install netcdf $LIMPIAR"
echo
if [ ! -d netcdf ];
then
    mkdir netcdf
fi

tar xzvf netcdf-4.1.3.tar.gz
cd netcdf-4.1.3
./configure --prefix=$DIR/netcdf --disable-dap --disable-netcdf-4 --disable-shared
make
make install
cd ..


echo
echo "$AZUL Install mpich $LIMPIAR"
echo
if [ ! -d mpich ];
then
    mkdir mpich
fi

tar xzvf mpich-3.0.4.tar.gz
cd mpich-3.0.4
./configure --prefix=$DIR/mpich
make
make install
cd ..


echo
echo "$AZUL Install zlib $LIMPIAR"
echo
if [ ! -d grib2 ];
then
    mkdir grib2
fi

tar xzvf zlib-1.2.7.tar.gz
cd zlib-1.2.7
./configure --prefix=$DIR/grib2
make
make install
cd ..


echo
echo "$AZUL Install libpng $LIMPIAR"
echo
tar xzvf libpng-1.2.50.tar.gz
cd libpng-1.2.50
./configure --prefix=$DIR/grib2
make
make install
cd ..


echo
echo "$AZUL Install Jasper $LIMPIAR"
echo
tar xzvf jasper-1.900.1.tar.gz
cd jasper-1.900.1
./configure --prefix=$DIR/grib2
make
make install
cd ..

cd ..

cd ..


echo
echo "$AZUL Libraries compatibility tests $LIMPIAR"
echo
cd /home/$usuario/TEST
wget http://www2.mmm.ucar.edu/wrf/OnLineTutorial/compile_tutorial/tar_files/Fortran_C_NETCDF_MPI_tests.tar
tar -xvf Fortran_C_NETCDF_MPI_tests.tar
cp ${NETCDF}/include/netcdf.inc .

echo "$LILA Test 1: Fortran + C + NetCDF. $LIMPIAR"
gfortran -c 01_fortran+c+netcdf_f.f
gcc -c 01_fortran+c+netcdf_c.c
gfortran 01_fortran+c+netcdf_f.o 01_fortran+c+netcdf_c.o -L${NETCDF}/lib -lnetcdff -lnetcdf
./a.out

echo "$LILA Test 2: Fortran + C + NetCDF + MPI. $LIMPIAR"
mpif90 -c 02_fortran+c+netcdf+mpi_f.f
mpicc -c 02_fortran+c+netcdf+mpi_c.c
mpif90 02_fortran+c+netcdf+mpi_f.o 02_fortran+c+netcdf+mpi_c.o -L${NETCDF}/lib -lnetcdff -lnetcdf
mpirun ./a.out

cd ..


echo
echo "$VERDE=================================================$LIMPIAR"
echo "$VERDE=                     END                       =$LIMPIAR"
echo "$VERDE=================================================$LIMPIAR"
echo

exit 0
