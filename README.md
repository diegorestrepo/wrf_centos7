# Configure CentOS 7 to compile WRF

The install_wrf.sh script prepares CentOS 7 to install WRF. It has been tested with WRFv4.0.

## Step 1:
Download WRF and WSP, save them in /home/you: https://www2.mmm.ucar.edu/wrf/users/download/get_sources.html

## Step 2:
Grant execute permissions to script:
>>>
chmod u+xr install_wrf.sh
>>>

## Step 3:
### Run script:
root:
>>>
./install_wrf.sh
>>>
or user:
>>>
sudo ./install_wrf.sh
>>>

## Step 4:
>>>
source /home/you/.bashrc
>>>


## References:
- How to Compile WRF (The Complete Process): https://www2.mmm.ucar.edu/wrf/OnLineTutorial/compilation_tutorial.php
- WRFv-4.1.2 Configure on CentOS 8: https://github.com/agumartina/WRFv-4.1.2-Configure-on-CentOS8
- Install WRF 3.8.1 on PC with CentOS 7(1511): http://k75789.blogspot.com/2016/10/install-wrf-381-on-pc-with-centos-71511.html
